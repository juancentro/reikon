﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThomsonReuters.Desktop.SDK.DataAccess;
using ThomsonReuters.Desktop.SDK.DataAccess.Common;
using ThomsonReuters.Desktop.SDK.DataAccess.Realtime;
using ThomsonReuters.Desktop.SDK.DataAccess.TimeSeries;

namespace reikon
{
    public class Eikon
    {
        private static Dictionary<string, SingleFieldValue> suscriptions;
        private static Dictionary<string, EikonTimeSerie> histories;
        private static List<IRealtimeSingleDataSubscription> antigc;

        public static void Subscribe(string ric, string field)
        {
            var s = DataServices.Instance.Realtime.Subscribe(ric, field, x => suscriptions[Key(ric, field)] = x);
            antigc.Add(s);
        }

        private static string Key(string ric, string field)
        {
            return string.Concat(ric, "|", field);
        }

        public static double GetZero() { return 0; }

        public static double? GetValue(string ric, string field)
        {
            SingleFieldValue value;
            var isThere = suscriptions.TryGetValue(Key(ric, field), out value);
            if (isThere)
                return value.ToDouble();
            else
                return null;        
        }

        public static EikonTimeSerie GetHistory(string ric)
        {
            EikonTimeSerie ts;
            var isThere = histories.TryGetValue(ric, out ts);
            if (isThere && ts != null && ts.isReady)
                return ts;
            else
                return null;
        }

        public static void RequestHistory(string ric, DateTime? from, DateTime? to, int freq)
        {
            var interval = (CommonInterval) freq;
            histories.Remove(ric);
            DataServices.Instance.TimeSeries.SetupDataRequest(ric)                
                .From(from)
                .To(to)
                .WithInterval(interval)
                .OnDataReceived(OnHistoryDataReceived)                
                .CreateAndSend();
        }

        private static void OnHistoryDataReceived(DataChunk chunk)
        {
            EikonTimeSerie ts;
            var isThere = histories.TryGetValue(chunk.Ric, out ts);
            if (!isThere)
            {
                ts = new EikonTimeSerie();
                histories[chunk.Ric] = ts;
            }
            ts.AddChunk(chunk);
        }

        public static void Init()
        {            
            DataServices.Instance.Initialize("reikon");
            suscriptions = new Dictionary<string, SingleFieldValue>();
            histories = new Dictionary<string, EikonTimeSerie>();
            antigc = new List<IRealtimeSingleDataSubscription>();
        }

        public static void Terminate()
        {
            antigc.ForEach(x => x.Dispose()); 
            DataServices.Instance.Dispose();
        }
    }
}
